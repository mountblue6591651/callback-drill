const fsProblem1 = require('../fs-problem1.cjs');
const path = require('path');

let absolutePathOfRandomDirectory = path.join(__dirname,'RandomJSONs');

let randomNumberOfFiles = 5;

fsProblem1(absolutePathOfRandomDirectory, randomNumberOfFiles)
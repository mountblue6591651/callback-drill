const fs = require("fs");
const path = require('path');

function createFiles(absolutePathOfRandomDirectory,randomNumberOfFiles) {
  fs.mkdir(absolutePathOfRandomDirectory, (error) => {
    if (error) {
      console.error(error);
    } else {
      console.log("Directory Created");
      for (let index = 1; index <= randomNumberOfFiles; index++) {
        let data = JSON.stringify({ key: `${index}` });
        let filePath = path.join(absolutePathOfRandomDirectory,`file${index}.json`)
        fs.writeFile(filePath, data, (err) => {
          if (err) {
            console.error(err);
          } else {
            console.log(`File${index} created`);

            fs.rm(filePath, (err) => {
              if (err) {
                console.error(err);
              } else {
                console.log(`File${index} Deleted`);
              }
            });
          }
        });
      }
    }
  });
}

module.exports = createFiles;

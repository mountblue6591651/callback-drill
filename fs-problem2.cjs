const fs = require("fs");

function fsProblem2() {
  // Step 1: Read the content of the lipsum_1.txt file
  fs.readFile("./lipsum_1.txt", "utf-8", (error, data) => {
    if (error) {
      console.error(error);
    } else {
      // Step 2: Create a new file (UpperCaseFile.txt) with the content in uppercase
      fs.writeFile("./upperCaseFile.txt", data.toUpperCase(), (err) => {
        if (err) {
          console.error(err);
        } else {
          console.log("UpperCaseFile Created");
          
          // Step 3: Append the filename to the filenames.txt file
          fs.appendFile("./filenames.txt", `upperCaseFile.txt\n`, (error) => {
            if (error) {
              console.error(error);
            } else {
              // Step 4: Read the content of the UpperCaseFile.txt
              fs.readFile("./upperCaseFile.txt", "utf-8", (err, data) => {
                if (err) {
                  console.error(err);
                } else {
                  // Step 5: Convert the content to lowercase, split into sentences, and join with newline
                  let sentences = data.toLowerCase().split(".").map((sentence) => sentence.trim()).join("\n");
                  
                  // Step 6: Append the processed content to the lowerCaseSentences.txt file
                  fs.appendFile("./lowerCaseSentences.txt", sentences, (error) => {
                    if (error) {
                      console.error(error);
                    } else {
                      console.log("LowerCaseSentences file created");
                      
                      // Step 7: Append the filename to the filenames.txt file
                      fs.appendFile("filenames.txt", `lowerCaseSentences.txt\n`, (err) => {
                        if (err) {
                          console.error(error);
                        } else {
                          // Step 8: Read the content of the lowerCaseSentences.txt
                          fs.readFile("./lowerCaseSentences.txt", "utf-8", (err, data2) => {
                            if (err) {
                              console.error(err);
                            } else {
                              // Step 9: Combine the content of UpperCaseFile.txt and lowerCaseSentences.txt, sort, and join with newline
                              let totalData = (data + data2).split("\n").sort().join("\n");
                              
                              // Step 10: Write the sorted content to the sortedFile.txt
                              fs.writeFile("./sortedFile.txt", totalData, (err) => {
                                if (err) {
                                  console.error(err);
                                } else {
                                  console.log("SortedFile Created");
                                  
                                  // Step 11: Append the filename to the filenames.txt file
                                  fs.appendFile("filenames.txt", `sortedFile.txt\n`, (err) => {
                                    if (err) {
                                      console.error(error);
                                    } else {
                                      // Step 12: Read the content of the filenames.txt file
                                      fs.readFile('./filenames.txt', 'utf-8', (err,data) => {
                                        if (err) {
                                          console.error(err);
                                        } else {
                                          // Step 13: Extract filenames, delete corresponding files, and print success message
                                          let files = data.split('\n').map((file) => file.trim());

                                          for (let index = 0; index < files.length; index++) {
                                            fs.rm(`./${files[index]}`, (err) => {
                                              if (err) {
                                                console.error(err);
                                              } else {
                                                console.log(`${files[index]} deleted`);
                                              }
                                            });
                                          }
                                        }
                                      });
                                    }
                                  });
                                }
                              });
                            }
                          });
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
}

module.exports = fsProblem2;
